tinywm-ruby
===========

This is translation of [tinywm](https://github.com/mackstann/tinywm) to Ruby.
More information of original tinywm at http://incise.org/tinywm.html.

I've try to translate C to Ruby as 1:1 as possible, that you can bootstrap with
xlib Ruby library in C way - there is more resources about WM's in C than Ruby
;) and unfortunately to much resources in Brainfuck...yyy I mean in C++.

tinywm usage
------------

Tinywm is realy tiny, its features:
* `Alt + Button1` - move window
* `Alt + Button2` - resize window
* `Alt + F1` - bring window on top

How to play with tinywm-ruby
----------------------------

Instal Xephyr - it can run X in a window in your current X session.

Run nested X:

```sh
Xephyr -br -ac -noreset -screen 640x480 :3
```

Run some program in it:

```sh
DISPLAY=:3 xterm
```

You can use xterm to run more programs ;)

There is no WM there yet, so you can not move anything.

Run tinywm-ruby:

```sh
DISPLAY=:3 ruby tinywm.rb
```

Tada!

> To kill tinywm.rb press `Ctrl + C` (in terminal where you run it) and then
> try to move some window (or any action supported by tinywm-ruby) because
> most of the time program is locked on
> `Xlib.XNextEvent display, event`

More
----

There is a lot of useful comments in original C version of tinywm,
file: annotated.c

---
Happy Hacking!
