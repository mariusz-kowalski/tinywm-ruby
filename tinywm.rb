require 'xlib'

def max(a, b)
  a > b ? a : b
end

attributes = Xlib::WindowAttributes.new
start = Xlib::XButtonEvent.new
# event = Xlib::XEvent.new # event must be created in each iteration of loop

display_pointer = Xlib.XOpenDisplay(ENV['DISPLAY'])
display = Xlib::Display.new(display_pointer)
# @ToDo: check if display is OK

Xlib.XGrabKey(
  display,
  Xlib.XKeysymToKeycode(display, Xlib.XStringToKeysym('F1')),
  Xlib::Mod1Mask,
  Xlib.XDefaultRootWindow(display),
  true,
  Xlib::GrabModeAsync,
  Xlib::GrabModeAsync
)

Xlib.XGrabButton(
  display,
  1,
  Xlib::Mod1Mask,
  Xlib.XDefaultRootWindow(display),
  true,
  Xlib::ButtonPressMask | Xlib::ButtonReleaseMask | Xlib::PointerMotionMask,
  Xlib::GrabModeAsync,
  Xlib::GrabModeAsync,
  Xlib::None,
  Xlib::None
)

Xlib.XGrabButton(
  display,
  3,
  Xlib::Mod1Mask,
  Xlib.XDefaultRootWindow(display),
  true,
  Xlib::ButtonPressMask | Xlib::ButtonReleaseMask | Xlib::PointerMotionMask,
  Xlib::GrabModeAsync,
  Xlib::GrabModeAsync,
  Xlib::None,
  Xlib::None
)

start[:subwindow] = Xlib::None

loop do
  event = Xlib::XEvent.new
  Xlib.XNextEvent display, event

  if event[:type] == Xlib::KeyPress && event[:xkey][:subwindow] != Xlib::None
    Xlib.XRaiseWindow display, event[:xkey][:subwindow]
  elsif event[:type] == Xlib::ButtonPress && event[:xbutton][:subwindow] != Xlib::None
    Xlib.XGetWindowAttributes(display, event[:xbutton][:subwindow], attributes)
    start = event[:xbutton]
  elsif event[:type] == Xlib::MotionNotify && start[:subwindow] != Xlib::None
    xdiff = event[:xbutton][:x_root] - start[:x_root]
    ydiff = event[:xbutton][:y_root] - start[:y_root]
    Xlib.XMoveResizeWindow(
      display,
      start[:subwindow],
      attributes[:x] + (start[:button] == 1 ? xdiff : 0),
      attributes[:y] + (start[:button] == 1 ? ydiff : 0),
      max(1, attributes[:width] + (start[:button] == 3 ? xdiff : 0)),
      max(1, attributes[:height] + (start[:button] == 3 ? ydiff : 0))
    )
  elsif event[:type] == Xlib::ButtonRelease
    start[:subwindow] = Xlib::None
  end
end
